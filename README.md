Extrait des forêts publiques, autour de Maubeuge

Source: [FOR_PUBL_FR](https://geo.data.gouv.fr/fr/datasets/fac934f5b6934af22dc56b1651e02f5dbda782c6)

Cutted using `mapshaper`:

        omapshaper -i ~/Downloads/FOR_PUBL_FR.json -clip bbox=3.8246,49.8880,4.4186,50.3043 -o forets-autour-aibes.json
